﻿namespace MSProject
{
    partial class MGForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.LogLabelTitle = new System.Windows.Forms.Label();
            this.LogLabel = new System.Windows.Forms.Label();
            this.RunNumberTitleLabel = new System.Windows.Forms.Label();
            this.RunNumberLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LogLabelTitle
            // 
            this.LogLabelTitle.AutoSize = true;
            this.LogLabelTitle.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.LogLabelTitle.Location = new System.Drawing.Point(14, 10);
            this.LogLabelTitle.Name = "LogLabelTitle";
            this.LogLabelTitle.Size = new System.Drawing.Size(90, 20);
            this.LogLabelTitle.TabIndex = 0;
            this.LogLabelTitle.Text = "Bot Status:";
            // 
            // LogLabel
            // 
            this.LogLabel.AutoSize = true;
            this.LogLabel.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.LogLabel.Location = new System.Drawing.Point(14, 40);
            this.LogLabel.Name = "LogLabel";
            this.LogLabel.Size = new System.Drawing.Size(41, 20);
            this.LogLabel.TabIndex = 1;
            this.LogLabel.Text = "Idle.";
            // 
            // RunNumberTitleLabel
            // 
            this.RunNumberTitleLabel.AutoSize = true;
            this.RunNumberTitleLabel.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.RunNumberTitleLabel.Location = new System.Drawing.Point(341, 10);
            this.RunNumberTitleLabel.Name = "RunNumberTitleLabel";
            this.RunNumberTitleLabel.Size = new System.Drawing.Size(106, 20);
            this.RunNumberTitleLabel.TabIndex = 2;
            this.RunNumberTitleLabel.Text = "RunNumber:";
            // 
            // RunNumberLabel
            // 
            this.RunNumberLabel.AutoSize = true;
            this.RunNumberLabel.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.RunNumberLabel.Location = new System.Drawing.Point(447, 10);
            this.RunNumberLabel.Name = "RunNumberLabel";
            this.RunNumberLabel.Size = new System.Drawing.Size(18, 20);
            this.RunNumberLabel.TabIndex = 3;
            this.RunNumberLabel.Text = "0";
            // 
            // MGForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(654, 73);
            this.Controls.Add(this.RunNumberLabel);
            this.Controls.Add(this.RunNumberTitleLabel);
            this.Controls.Add(this.LogLabel);
            this.Controls.Add(this.LogLabelTitle);
            this.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MGForm";
            this.Opacity = 0.8D;
            this.ShowIcon = false;
            this.Text = "MapleGhost";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LogLabelTitle;
        private System.Windows.Forms.Label LogLabel;
        private System.Windows.Forms.Label RunNumberTitleLabel;
        private System.Windows.Forms.Label RunNumberLabel;
    }
}

