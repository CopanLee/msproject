﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using MSProject.Library.WindowInfo;
using MSProject.Script;
using static MSProject.Script.ScriptManager;
using System.Threading;

namespace MSProject.Library
{
    class MiniMap
    {
        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr handle);
        [DllImport("user32.dll")]
        private static extern Int32 ReleaseDC(IntPtr handle, IntPtr hdc);

        public static Color CatchMiniMapPositionColor(int X, int Y)
        {
            Point indexPoint = new Point
            {
                X = X,
                Y = Y
            };
            using (var screenPixel = new Bitmap(1, 1))
            {
                using (var graphics = Graphics.FromImage(screenPixel))
                {
                    graphics.CopyFromScreen(indexPoint, new Point(0, 0), new Size(1, 1));
                }
                return screenPixel.GetPixel(0, 0);
            }
        }

        public static bool IsPlayerArriveAt(int X, int Y)
        {
            int R = 255, G = 221, B = 68;
            Color MiniMapColor = CatchMiniMapPositionColor(X, Y);
            if (MiniMapColor.R == R && MiniMapColor.G == G && MiniMapColor.B == B) return true;
            else return false;
        }

        public static bool IsPlayerArriveAt(int X, int Y, DelegateScript Script)
        {
            int R = 255, G = 221, B = 68;
            Color MiniMapColor = CatchMiniMapPositionColor(X, Y);
            if (MiniMapColor.R == R && MiniMapColor.G == G && MiniMapColor.B == B)
            {
                Script();
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool NpcCheck(int X, int Y)
        {
            int R = 0, G = 221, B = 0;
            Color MiniMapColor = CatchMiniMapPositionColor(X, Y);
            return (MiniMapColor.R == R && MiniMapColor.G == G && MiniMapColor.B == B) ? true : false;
        }

        public static bool PixelRGBCheck(int X, int Y, int R, int G, int B)
        {
            Color MiniMapColor = CatchMiniMapPositionColor(X, Y);
            return (MiniMapColor.R == R && MiniMapColor.G == G && MiniMapColor.B == B) ? true : false;
        }

        public static bool MapEnterCheck(int X, int Y, int R, int G, int B)
        {
            int counter = 0;

            while (counter++ < 20)
            {
                Color MiniMapColor = CatchMiniMapPositionColor(X, Y);
                if (MiniMapColor.R != R && MiniMapColor.G != G && MiniMapColor.B != B) return true;
                Thread.Sleep(50);
            }
            return false;
        }
    }
}
