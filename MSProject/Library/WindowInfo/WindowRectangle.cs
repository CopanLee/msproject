﻿using System.Runtime.InteropServices;

namespace MSProject.Library.WindowInfo
{
    [StructLayout(LayoutKind.Sequential)]
    public struct WindowRectangle
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
    }
}
