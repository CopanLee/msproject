﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using MSProject.Model;

namespace MSProject.Library.WindowInfo
{
    public static class GetWindowInfo
    {
        [DllImport("user32.dll")]
        private static extern IntPtr FindWindow(string className, string FormName);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowPlacement(IntPtr handle, ref WindowPlacement windowPlacement);
        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr handle);

        public static Maplestory GetMaplestoryInfo()
        {
            var handle = FindWindow(null, "Maplestory");
            var windowPlacement = new WindowPlacement();

            GetWindowPlacement(handle, ref windowPlacement);
            SetForegroundWindow(handle);
            return new Maplestory(handle, windowPlacement.WindowRectangle);
        }
    }
}
