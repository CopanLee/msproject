﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSProject.Library.WindowInfo
{
    struct WindowPlacement
    {
        public int Length;
        public int Mode;
        public int Flag;
        private Point MinimizePoint;
        private Point MaximizePoint;
        public WindowRectangle WindowRectangle;
    }
}
