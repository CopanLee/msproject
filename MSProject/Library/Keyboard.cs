﻿using MSProject.Script;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MSProject.Library.InputSimulator;

namespace MSProject.Library
{
    class Keyboard
    {

        public static void Down(IntPtr handle, Keys key)
        {
            IntPtr lParam = new IntPtr((MapVirtualKey(SwitchKey(key), 0) << 16) + 1);
            PostMessage(handle, 0x100, (IntPtr)key, lParam);
        }

        public static void Up(IntPtr handle, Keys key)
        {
            IntPtr lParam = new IntPtr((MapVirtualKey(SwitchKey(key), 0) << 16) + 1);
            PostMessage(handle, 0x101, (IntPtr)key, lParam);
        }

        public static void DirectionDown(Keys key)
        {
            var input = new Input
            {
                Type = SendInputEventType.InputKeyboard
            };

            input.Union.KeyboardInputData.Flag = KeyboardEventFlags.KEYBDEVENTF_EXTENDEDKEY;
            input.Union.KeyboardInputData.VirtualKey = SwitchKey(key);
            SendInput(1, ref input, Marshal.SizeOf(new Input()));
        }

        public static void DirectionUp(Keys key)
        {
            var input = new Input
            {
                Type = SendInputEventType.InputKeyboard
            };

            input.Union.KeyboardInputData.Flag = KeyboardEventFlags.KEYBDEVENTF_KEYUP | KeyboardEventFlags.KEYBDEVENTF_EXTENDEDKEY;
            input.Union.KeyboardInputData.VirtualKey = SwitchKey(key);
            SendInput(1, ref input, Marshal.SizeOf(new Input()));
        }

        public static void Press(IntPtr handle, Keys key, int time)
        {
            Down(handle, key);
            Thread.Sleep(time);
            Up(handle, key);
        }

        public static void DirectionPress(Keys key, int time)
        {
            DirectionDown(key);
            Thread.Sleep(time);
            DirectionUp(key);
        }


    }
}
