﻿using System;
using MSProject.Library.WindowInfo;

namespace MSProject.Model
{
    public class Maplestory
    {
        public IntPtr Handle { get; private set; }
        public WindowRectangle WindowRectangle { get; private set; }

        public Maplestory(IntPtr handle, WindowRectangle rectangle)
        {
            Handle = handle;
            WindowRectangle = rectangle;
        }
    }
}
