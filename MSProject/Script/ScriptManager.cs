﻿using System.Threading;

namespace MSProject.Script
{
    public partial class ScriptManager
    {
        #region Status Member
        public bool IsBotting { get; private set; }

        public delegate void DelegateScript();

        private Thread scriptThread;
        private MGForm mGForm;
        #endregion

        public ScriptManager(MGForm mGForm)
        {
            this.mGForm = mGForm;
        }

        public void ScriptChoose(Script ScriptType)
        {
            switch (ScriptType)
            {
                case Script.KnightStronghold:
                    if (IsBotting) IsBotting = false;
                    else
                    {
                        IsBotting = true;
                        scriptThread = new Thread(KnightStronghold);
                        scriptThread.Start();
                    }
                    break;
                case Script.DreamyForestTrail:
                    if (IsBotting) IsBotting = false;
                    else
                    {
                        IsBotting = true;
                        scriptThread = new Thread(DreamyForestTrail);
                        scriptThread.Start();
                    }
                    break;
            }
        }
    }
}
