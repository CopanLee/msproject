﻿using MSProject.Library;
using MSProject.Model;
using MSProject.Library.WindowInfo;
using System.Windows.Forms;

namespace MSProject.Script
{
    public partial class ScriptManager
    {
        public void DreamyForestTrail()
        {
            var maplestory = GetWindowInfo.GetMaplestoryInfo();

            if (maplestory.WindowRectangle.Left == 0 && maplestory.WindowRectangle.Top == 0)
            {
                mGForm.ShowMessageBox("Window catch error", "please check the game is running.");
                return;
            }

            int runTime = 1;

            while (IsBotting)
            {
                mGForm.UpdateRunNumberLabel(runTime++.ToString());
                TopPlatform(maplestory);
                BottomPlatform(maplestory);
            }
        }

        public void TopPlatform(Maplestory maplestory)
        {
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Wait until the character climb rope top");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 146, maplestory.WindowRectangle.Top + 129, () => Climb(false)))
            {
                Climb(true);
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Wait until the character attack.");
                OneKeyAttack(maplestory.Handle, Keys.Right, Keys.Control, 300);
                Delay(500);
                OneKeyAttack(maplestory.Handle, Keys.Left, Keys.Control, 300);
            }
            else
            {
                return;
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Wait until the character arrive platform left side and jump.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 127, maplestory.WindowRectangle.Top + 125, () => Left(false)))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 130, maplestory.WindowRectangle.Top + 129, () => Left(false)))
                {
                    Jump(maplestory.Handle, 300);
                }
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 133, maplestory.WindowRectangle.Top + 129, () => Left(false)))
                {
                    Jump(maplestory.Handle, 300);
                }
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 87, maplestory.WindowRectangle.Top + 148, () => Left(false)))
                {
                    return;
                }
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 84, maplestory.WindowRectangle.Top + 148, () => Left(false)))
                {
                    return;
                }
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 81, maplestory.WindowRectangle.Top + 148, () => Left(false)))
                {
                    return;
                }
                else
                {
                    Left(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Wait until the character arrive the platform 2.2 left side.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 81, maplestory.WindowRectangle.Top + 125))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 84, maplestory.WindowRectangle.Top + 125))
                {
                    break;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Left, Keys.Shift, 200);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Wait until the character arrive left wall.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 40, maplestory.WindowRectangle.Top + 141, () => Left(false)))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 21, maplestory.WindowRectangle.Top + 141, () => Left(false)))
                {
                    return;
                }
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 84, maplestory.WindowRectangle.Top + 125))
                {
                    JumpJump(maplestory.Handle, 0, 0);
                    OneKeyAttack(maplestory.Handle, 0x00, Keys.D, 500);
                }
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 51, maplestory.WindowRectangle.Top + 144, () => Left(false)))
                {
                    return;
                }
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 66, maplestory.WindowRectangle.Top + 144, () => Left(false)))
                {
                    return;
                }
                else
                {
                    Left(true);
                }
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 9, maplestory.WindowRectangle.Top + 141))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 11, maplestory.WindowRectangle.Top + 141))
                {
                    break;
                }
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 14, maplestory.WindowRectangle.Top + 141))
                {
                    break;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Left, Keys.Shift, 200);
                }
            }
            return;
        }

        public void BottomPlatform(Maplestory maplestory)
        {
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Wait until the character arrive the right wall.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 166, maplestory.WindowRectangle.Top + 148))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 163, maplestory.WindowRectangle.Top + 148))
                {
                    break;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Right, Keys.Shift, 200);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Wait until the character climb the rope.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 146, maplestory.WindowRectangle.Top + 147, () => Left(false)))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 146, maplestory.WindowRectangle.Top + 143, () => Left(false)))
                {
                    break;
                }
                else
                {
                    Left(true);
                    Climb(true);
                }
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 146, maplestory.WindowRectangle.Top + 137, () => Climb(false)))
            {
                Climb(true);
            }
            return;
        }
    }
}
