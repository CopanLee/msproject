﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MSProject.Library;
using System.Windows.Forms;

namespace MSProject.Script
{
    public partial class ScriptManager
    {
        public void Delay(int delayTime)
        {
            Thread.Sleep(delayTime);
        }

        public void Up()
        {
            Keyboard.DirectionPress(Keys.Up, 0);
        }

        public void Up(int time, int delayTime)
        {
            Keyboard.DirectionPress(Keys.Up, time);
            Thread.Sleep(delayTime);
        }

        public void Down(int time, int delayTime)
        {
            Keyboard.DirectionPress(Keys.Down, time);
            Thread.Sleep(delayTime);
        }

        public void Enter(int time)
        {
            Keyboard.DirectionPress(Keys.Enter, time);
        }

        public void Left(bool state)
        {
            if (state) Keyboard.DirectionDown(Keys.Left);
            else Keyboard.DirectionUp(Keys.Left);
        }

        public void Left(int time)
        {
            Keyboard.DirectionDown(Keys.Left);
            Thread.Sleep(time);
            Keyboard.DirectionUp(Keys.Left);
        }

        public void Left(int time, int delayTime)
        {
            Keyboard.DirectionDown(Keys.Left);
            Thread.Sleep(time);
            Keyboard.DirectionUp(Keys.Left);
            Thread.Sleep(delayTime);
        }

        public void Right(bool state)
        {
            if (state) Keyboard.DirectionDown(Keys.Right);
            else Keyboard.DirectionUp(Keys.Right);
        }

        public void Right(int time)
        {
            Keyboard.DirectionDown(Keys.Right);
            Thread.Sleep(time);
            Keyboard.DirectionUp(Keys.Right);
        }

        public void Right(int time, int delayTime)
        {
            Keyboard.DirectionDown(Keys.Right);
            Thread.Sleep(time);
            Keyboard.DirectionUp(Keys.Right);
            Thread.Sleep(delayTime);
        }

        public void Jump(IntPtr handle, int delayTime)
        {
            Keyboard.Press(handle, Keys.Alt, 100);
            Thread.Sleep(delayTime);
        }

        public void JumpJump(IntPtr handle, int jumpimtervaltime, int delayTime)
        {
            Keyboard.Press(handle, Keys.Alt, 50);
            Thread.Sleep(jumpimtervaltime);
            Keyboard.Press(handle, Keys.Alt, 50);
            Thread.Sleep(delayTime);
        }

        public void JumpDown(IntPtr handle, int delayTime)
        {
            Keyboard.DirectionDown(Keys.Down);
            Thread.Sleep(200);
            Keyboard.Press(handle, Keys.Alt, 200);
            Thread.Sleep(200);
            Keyboard.DirectionUp(Keys.Down);
            Thread.Sleep(delayTime);
        }

        public void Climb(bool state)
        {
            if (state) Keyboard.DirectionDown(Keys.Up);
            else
            {
                Keyboard.DirectionUp(Keys.Up);
                Keyboard.DirectionPress(Keys.Up, 100);
            }
        }

        public void OneKeyAttackWhenWalk(IntPtr handle, Keys DirectionKey, Keys KeyOne, int WalkTime, bool state)
        {
            if (state && !MiniMap.NpcCheck(70, 100))
            {
                Keyboard.Press(handle, KeyOne, 50);
                Keyboard.DirectionDown(DirectionKey);
                Thread.Sleep(WalkTime);
                Keyboard.DirectionUp(DirectionKey);
            }
            else
            {
                Keyboard.DirectionUp(DirectionKey);
                Keyboard.Up(handle, KeyOne);
            }
        }

        public void OneKeyAttack(IntPtr handle, Keys DirectionKey, Keys KeyOne, int delayTime)
        {
            if (!MiniMap.NpcCheck(70, 100))
            {
                if (DirectionKey != 0x00) Keyboard.DirectionPress(DirectionKey, 50);
                Keyboard.Press(handle, KeyOne, 50);
                Thread.Sleep(delayTime);
            }
        }

        public void TwoKeyAttack(IntPtr handle, Keys DirectionKey, Keys KeyOne, Keys keyTwo, int intervalOne, int delayTime)
        {
            if (!MiniMap.NpcCheck(70, 100))
            {
                Keyboard.DirectionPress(DirectionKey, 50);
                Keyboard.Press(handle, KeyOne, 50);
                Thread.Sleep(intervalOne);
                Keyboard.Press(handle, keyTwo, 50);
                Thread.Sleep(delayTime);
            }
        }
    }
}
