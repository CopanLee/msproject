﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSProject.Library;
using MSProject.Model;
using MSProject.Library.WindowInfo;
using System.Threading;
using System.Windows.Forms;

namespace MSProject.Script
{
    public partial class ScriptManager
    {
        public void KnightStronghold()
        {
            var maplestory = GetWindowInfo.GetMaplestoryInfo();

            if (maplestory.WindowRectangle.Left == 0 && maplestory.WindowRectangle.Top == 0)
            {
                mGForm.ShowMessageBox("Window catch error", "please check the game is running.");
                return;
            }

            int runTime = 1;

            while (IsBotting)
            {
                mGForm.UpdateRunNumberLabel(runTime++.ToString());
                EnterKnightStronghold(maplestory, 5);
                KnightStrongholdStage1(maplestory);
                KnightStrongholdStage2(maplestory);
                KnightStrongholdStage3(maplestory);
                KnightStrongholdStage4(maplestory);
                KnightStrongholdStage5(maplestory);
                KnightStrongholdStage6(maplestory);
            }
        }

        public void EnterKnightStronghold(Maplestory maplestory, int WhichLevel)
        {
            var MiniMapLeftStartPoint = 0;
            var EnterCounter = 0;

            CharacterWalk:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Wait until the Character position calculation completed.");
            }
            else
            {
                return;
            }
            do
            {
                MiniMapLeftStartPoint = 36;
                for (; MiniMapLeftStartPoint <= 143; MiniMapLeftStartPoint++) if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + MiniMapLeftStartPoint, maplestory.WindowRectangle.Top + 122)) break;
            } while (IsBotting && MiniMapLeftStartPoint == 144);
            if (MiniMapLeftStartPoint > 128)
            {
                if (IsBotting)
                {
                    mGForm.UpdateLogLabel("(L)Wait until the character arrive the portal.");
                }
                else
                {
                    return;
                }
                while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 128, maplestory.WindowRectangle.Top + 122, () => Left(false)))
                {
                    if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 34, maplestory.WindowRectangle.Top + 122, () => Left(false)))
                    {
                        goto CharacterWalk;
                    }
                    else
                    {
                        Left(true);
                    }
                }
            }
            if (MiniMapLeftStartPoint < 127)
            {
                if (IsBotting)
                {
                    mGForm.UpdateLogLabel("(R)Wait until the character arrive the portal.");
                }
                else
                {
                    return;
                }
                while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 127, maplestory.WindowRectangle.Top + 122, () => Right(false)))
                {
                    if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 148, maplestory.WindowRectangle.Top + 122, () => Right(false)))
                    {
                        goto CharacterWalk;
                    }
                    else
                    {
                        Right(true);
                    }
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Wait until the character enter the portal");
                Delay(500);
            }
            else
            {
                return;
            }
            while (IsBotting && MiniMap.PixelRGBCheck(maplestory.WindowRectangle.Left + 127, maplestory.WindowRectangle.Top + 118, 204, 255, 255))
            {
                if (EnterCounter++ > 10)
                {
                    EnterCounter = 0;
                    goto CharacterWalk;
                }
                if (MiniMap.PixelRGBCheck(maplestory.WindowRectangle.Left + 909, maplestory.WindowRectangle.Top + 508, 187, 238, 0))
                {
                    break;
                }
                while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 131, maplestory.WindowRectangle.Top + 122))
                {
                    EnterCounter = 0;
                    Left(50, 300);
                }
                while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 124, maplestory.WindowRectangle.Top + 122))
                {
                    EnterCounter = 0;
                    Right(50, 300);
                }
                Up(0, 1000);
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Selecting level...");
                for (int currentCount = 0; currentCount < WhichLevel; currentCount++) Down(100, 220);
            }
            else
            {
                return;
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("Wait until the enter Map.");
            }
            else
            {
                return;
            }
            while (IsBotting && MiniMap.PixelRGBCheck(maplestory.WindowRectangle.Left + 909, maplestory.WindowRectangle.Top + 508, 187, 238, 0))
            {
                Enter(100);
            }
        }

        public void KnightStrongholdStage1(Maplestory maplestory)
        {
            var EnterCounter = 0;

            PartOne:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage1: Wait until enter the map.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 47, maplestory.WindowRectangle.Top + 143))
            {
                Delay(5);
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage1: Wait until the character arrive the right wall 1.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 206, maplestory.WindowRectangle.Top + 139))
            {
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 137, maplestory.WindowRectangle.Top + 143))
                {
                    OneKeyAttack(maplestory.Handle, 0x00, Keys.D, 500);
                }
                // Right wall 1 = (203, 139)
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 203, maplestory.WindowRectangle.Top + 139))
                {
                    break;
                }
                else
                {
                    TwoKeyAttack(maplestory.Handle, Keys.Right, Keys.Control, Keys.Shift, 600, 600);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage1: Wait until the character arrive the portal.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 188, maplestory.WindowRectangle.Top + 139, () => Left(false)))
            {
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 31, maplestory.WindowRectangle.Top + 143))
                {
                    goto Restart;
                }
                else
                {
                    Left(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage1 : Wait until the character enter the portal.");
                Delay(400);
            }
            else
            {
                return;
            }
            do
            {
                if (EnterCounter++ > 10)
                {
                    EnterCounter = 0;
                    goto Restart;
                }
                else
                {
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 191, maplestory.WindowRectangle.Top + 139))
                    {
                        EnterCounter = 0;
                        Left(50, 100);
                    }
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 184, maplestory.WindowRectangle.Top + 139))
                    {
                        EnterCounter = 0;
                        Right(50, 100);
                    }
                    Up();
                }
            } while (!MiniMap.MapEnterCheck(maplestory.WindowRectangle.Left + 38, maplestory.WindowRectangle.Top + 72, 102, 85, 34));
            return;

            Restart:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage1: Wait until the character arrive the left wall(re).");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 31, maplestory.WindowRectangle.Top + 143))
            {
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 34, maplestory.WindowRectangle.Top + 143))
                {
                    break;
                }
                else
                {
                    TwoKeyAttack(maplestory.Handle, Keys.Left, Keys.Control, Keys.Shift, 600, 600);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage1: Wait until the character arrive the spawn point(re).");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 46, maplestory.WindowRectangle.Top + 143, () => Right(false)))
            {
                Right(true);
            }
            goto PartOne;
        }

        public void KnightStrongholdStage2(Maplestory maplestory)
        {
            var EnterCounter = 0;
            
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2: Wait until enter the map.");
            }
            else
            {
                return;
            }//spawn point 69, 150
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 70, maplestory.WindowRectangle.Top + 150))
            {
                Delay(5);
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2: Wait until the character arrive the right wall 1.1.");
            }
            else
            {
                return;
            }
            PartOne:
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 161, maplestory.WindowRectangle.Top + 150))
            {
                //right wall 158, 150
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 158, maplestory.WindowRectangle.Top + 150))
                {
                    break;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Right, Keys.Shift, 300);
                }
            }

            PartTwo:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2: Wait until character climb the rope 1.");
            }
            else
            {
                return;
            }
            //rope 1 154, 147
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 154, maplestory.WindowRectangle.Top + 140, () => Left(false)))
            {
                //left wall 64, 150
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 64, maplestory.WindowRectangle.Top + 150, () => { Left(false); Climb(false); }))
                {
                    goto PartOne;
                }
                else
                {
                    Left(true);
                    Climb(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2: Wait until character arrive rope 1 top.");
            }
            else
            {
                Left(false);
                Climb(false);
                return;
            }
            //rope 1 top 154, 128
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 156, maplestory.WindowRectangle.Top + 128, () => { Climb(false); OneKeyAttack(maplestory.Handle, 0x00, Keys.D, 500); })) { }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2: Wait until character arrive platform left side.");
            }
            else
            {
                return;
            }
            //platform left side 112, 128
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 113, maplestory.WindowRectangle.Top + 128))
            {
                //left wall 64, 150
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 65, maplestory.WindowRectangle.Top + 150))
                {
                    goto PartOne;
                }
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 115, maplestory.WindowRectangle.Top + 128))
                {
                    break;
                }
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 117, maplestory.WindowRectangle.Top + 128))
                {
                    break;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle,Keys.Left, Keys.Shift, 300);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2: Wait until character climb rope 2.");
            }
            else
            {
                return;
            }
            //rope 2 109, 121
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 109, maplestory.WindowRectangle.Top + 118, () => Left(false)))
            {
                //left wall 65, 150
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 65, maplestory.WindowRectangle.Top + 150, () => { Left(false); Climb(false); }))
                {
                    goto PartOne;
                }
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 113, maplestory.WindowRectangle.Top + 128))
                {
                    Left(true);
                    Climb(true);
                    Jump(maplestory.Handle, 0);
                }
                else
                {
                    Left(true);
                    Climb(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2: Wait until character arrive rope 2 top.");
            }
            else
            {
                return;
            }
            //rope 2 top 109, 116
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 109, maplestory.WindowRectangle.Top + 117, () => Climb(false))) { }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2: Wait until character arrive top left wall.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 68, maplestory.WindowRectangle.Top + 117))
            {
                //top left wall 65, 117
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 65, maplestory.WindowRectangle.Top + 117))
                {
                    break;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Left, Keys.Shift, 300);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2: Wait until the character arrive the portal.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 77, maplestory.WindowRectangle.Top + 117, () => Right(false)))
            {
                //platform right wall 157, 128.
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 157, maplestory.WindowRectangle.Top + 128))
                {
                    JumpDown(maplestory.Handle, 1000);
                    goto PartTwo;
                }
                else
                {
                    Right(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2 : Wait until the character enter the portal.");
                OneKeyAttack(maplestory.Handle, 0x00, Keys.W, 200);
                Delay(400);
            }
            else
            {
                return;
            }
            do
            {
                if (EnterCounter++ > 10)
                {
                    EnterCounter = 0;
                    goto Restart;
                }
                else
                {
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 74, maplestory.WindowRectangle.Top + 117))
                    {
                        EnterCounter = 0;
                        Right(50, 100);
                    }
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 81, maplestory.WindowRectangle.Top + 117))
                    {
                        EnterCounter = 0;
                        Left(50, 100);
                    }
                    Up();
                }
            } while (!MiniMap.MapEnterCheck(maplestory.WindowRectangle.Left + 38, maplestory.WindowRectangle.Top + 72, 102, 85, 34));
            return;

            Restart:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2 : Wait until character arrive top left wall(re).");
            }
            else
            {
                return;
            }
            // top left wall 65, 117
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 65, maplestory.WindowRectangle.Top + 117, () => Left(false)))
            {
                Left(true);
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage2 : Wait until character jumpdown(re).");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 65, maplestory.WindowRectangle.Top + 150))
            {
                JumpDown(maplestory.Handle, 1200);
            }
            goto PartOne;
        }

        public void KnightStrongholdStage3(Maplestory maplestory)
        {
            var EnterCounter = 0;

            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage3: Wait until enter the map.");
            }
            else
            {
                return;
            }//spawn point 34, 126
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 35, maplestory.WindowRectangle.Top + 126))
            {
                Delay(5);
            }
            PartOne:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage3: Wait until the character arrive the tower.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 55, maplestory.WindowRectangle.Top + 126, () => OneKeyAttackWhenWalk(maplestory.Handle, Keys.Right, Keys.Control, 1500, false)))
            {
                // tower 55, 126
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 58, maplestory.WindowRectangle.Top + 126))
                {
                    break;
                }
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 61, maplestory.WindowRectangle.Top + 126))
                {
                    break;
                }
                // when mistake arrive right wall 169, 133
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 169, maplestory.WindowRectangle.Top + 133))
                {
                    goto PartTwo;
                }
                else
                {
                    OneKeyAttackWhenWalk(maplestory.Handle, Keys.Right, Keys.Control, 1500, true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage3: Wait until the character use F skill.");
                Delay(200);
                OneKeyAttack(maplestory.Handle, 0x00, Keys.D, 500);
            }
            else
            {
                return;
            }
            PartTwo:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage3: Wait until the character arrive right wall");
            }
            else
            {
                return;
            }
            // right wall 169, 133
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 169, maplestory.WindowRectangle.Top + 133, () => OneKeyAttackWhenWalk(maplestory.Handle, Keys.Right, Keys.Control, 1300, false)))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 166, maplestory.WindowRectangle.Top + 133, () => OneKeyAttackWhenWalk(maplestory.Handle, Keys.Right, Keys.Control, 1300, false)))
                {
                    break;
                }
                else
                {
                    OneKeyAttackWhenWalk(maplestory.Handle, Keys.Right, Keys.Control, 1300, true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage3: Wait until the character arrive the portal.");
            }
            else
            {
                return;
            }
            //portal 157, 133
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 157, maplestory.WindowRectangle.Top + 133, () => Left(false)))
            {
                //arrive stair 1 when mistake 93, 133
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 93, maplestory.WindowRectangle.Top + 133))
                {
                    Left(false);
                    goto PartTwo;
                }
                else
                {
                    Left(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage3 : Wait until the character enter the portal.");
                Delay(400);
            }
            else
            {
                return;
            }
            do
            {
                if (EnterCounter++ > 10)
                {
                    EnterCounter = 0;
                    goto Restart;
                }
                else
                {
                    // portal 161, 133
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 161, maplestory.WindowRectangle.Top + 133))
                    {
                        EnterCounter = 0;
                        Left(50, 100);
                    }
                    // portal 154, 133
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 154, maplestory.WindowRectangle.Top + 133))
                    {
                        EnterCounter = 0;
                        Right(50, 100);
                    }
                    Up();
                }
            } while (!MiniMap.MapEnterCheck(maplestory.WindowRectangle.Left + 38, maplestory.WindowRectangle.Top + 72, 102, 85, 34));
            return;

            Restart:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage3: Wait until the character arrive left wall(re)");
            }
            else
            {
                return;
            }
            //left wall 31, 126
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 31, maplestory.WindowRectangle.Top + 126, () => OneKeyAttackWhenWalk(maplestory.Handle, Keys.Left, Keys.Control, 0, false)))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 34, maplestory.WindowRectangle.Top + 126, () => OneKeyAttackWhenWalk(maplestory.Handle, Keys.Left, Keys.Control, 0, false)))
                {
                    goto PartOne;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Left, Keys.Shift, 300);
                }
            }
            goto PartOne;
        }

        public void KnightStrongholdStage4(Maplestory maplestory)
        {
            var EnterCounter = 0;

            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until enter the map.");
            }
            else
            {
                return;
            }//spawn point 124, 152
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 124, maplestory.WindowRectangle.Top + 152))
            {
                Delay(5);
            }
            PartOne:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until the character arrive the left wall 1.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 65, maplestory.WindowRectangle.Top + 152))
            {
                // left wall 1 = 62, 152
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 62, maplestory.WindowRectangle.Top + 152))
                {
                    break;
                }
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 60, maplestory.WindowRectangle.Top + 152))
                {
                    break;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Left, Keys.Shift, 300);
                }
            }
            PartTwo:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until character climb the rope 1.");
            }
            else
            {
                return;
            }
            //rope 1 72, 152
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 72, maplestory.WindowRectangle.Top + 145, () => Right(false)))
            {
                //when mistake arrive right wall 1.1 139, 152
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 139, maplestory.WindowRectangle.Top + 152, () => { Right(false); Climb(false); }))
                {
                    goto PartOne;
                }
                else
                {
                    Right(true);
                    Climb(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until character arrive rope 1 top.");
            }
            else
            {
                Left(false);
                Climb(false);
                return;
            }
            //rope 1 top 72, 129
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 72, maplestory.WindowRectangle.Top + 129, () => { Climb(false); OneKeyAttack(maplestory.Handle, 0x00, Keys.D, 500); })) { }

            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until the character arrive the platform1 left side.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 125, maplestory.WindowRectangle.Top + 129))
            {
                // when mistake arrive right wall 1.1 139, 152
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 139, maplestory.WindowRectangle.Top + 152))
                {
                    goto PartOne;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Right, Keys.Shift, 300);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until character climb the rope 2.");
            }
            else
            {
                return;
            }
            //rope 2 122, 125
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 122, maplestory.WindowRectangle.Top + 125, () => Left(false)))
            {
                //when mistake arrive left wall 2.1 63, 129
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 63, maplestory.WindowRectangle.Top + 129, () => { Left(false); Climb(false); }))
                {
                    JumpDown(maplestory.Handle, 1000);
                    goto PartTwo;
                }
                else
                {
                    Left(true);
                    Climb(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until character arrive rope 2 top.");
            }
            else
            {
                Left(false);
                Climb(false);
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 122, maplestory.WindowRectangle.Top + 107, () => { Climb(false); })) { }
            // platform 2
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until the character arrive the platform2 left side.");
                OneKeyAttack(maplestory.Handle, Keys.Left, Keys.Shift, 300);
            }
            else
            {
                return;
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until the character arrive the platform2 right side.");
                Right(200, 200);
                OneKeyAttack(maplestory.Handle, Keys.Right, Keys.Shift, 300);
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 131, maplestory.WindowRectangle.Top + 107, () => Right(false)))
            {
                // when mistake arrive right wall 1 = 139, 152
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 139, maplestory.WindowRectangle.Top + 152, () => Right(false)))
                {
                    goto PartOne;
                }
                else
                {
                    Right(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until the character enter the portal.");
                Delay(400);
            }
            else
            {
                return;
            }
            do
            {
                if (EnterCounter++ > 10)
                {
                    EnterCounter = 0;
                    goto Restart;
                }
                else
                {
                    // portal right side 135, 107
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 135, maplestory.WindowRectangle.Top + 107))
                    {
                        EnterCounter = 0;
                        Left(50, 100);
                    }
                    // portal left side 128, 107
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 128, maplestory.WindowRectangle.Top + 107))
                    {
                        EnterCounter = 0;
                        Right(50, 100);
                    }
                    Up();
                }
            } while (!MiniMap.MapEnterCheck(maplestory.WindowRectangle.Left + 38, maplestory.WindowRectangle.Top + 72, 102, 85, 34));
            return;

            Restart:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage4: Wait until the character arrive spawn point(re).");
                JumpDown(maplestory.Handle, 1500);
                goto PartOne;
            }
            else
            {
                return;
            }
        }

        public void KnightStrongholdStage5(Maplestory maplestory)
        {
            var EnterCounter = 0;

            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until enter the map.");
            }
            else
            {
                return;
            }
            //spawn point 72, 154
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 72, maplestory.WindowRectangle.Top + 154))
            {
                Delay(5);
            }
            PartOne:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until the character arrive the right wall 1.");
            }
            else
            {
                return;
            }
            // right wall 1 = 149, 124
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 149, maplestory.WindowRectangle.Top + 154))
            {
                OneKeyAttack(maplestory.Handle, Keys.Right, Keys.Shift, 300);
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until character climb the rope 1.");
            }
            else
            {
                return;
            }
            //rope 1 125, 146
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 125, maplestory.WindowRectangle.Top + 146, () => Left(false)))
            {
                //when mistake arrive spawn point 72, 154
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 72, maplestory.WindowRectangle.Top + 154, () => { Left(false); Climb(false); }))
                {
                    goto PartOne;
                }
                else
                {
                    Left(true);
                    Climb(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until character arrive rope 1 top.");
            }
            else
            {
                Left(false);
                Climb(false);
                return;
            }
            //rope 1 top 125, 132
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 125, maplestory.WindowRectangle.Top + 132, () => { Climb(false); OneKeyAttack(maplestory.Handle, 0x00, Keys.D, 500); })) { }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until the character arrive the left wall 2.1.");
            }
            else
            {
                return;
            }
            // right wall 1 = 75, 132
            PartTwo:
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 75, maplestory.WindowRectangle.Top + 132))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 78, maplestory.WindowRectangle.Top + 132))
                {
                    break;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Left, Keys.Shift, 300);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until character climb the rope 2.");
            }
            else
            {
                return;
            }
            //rope 2 85, 123
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 85, maplestory.WindowRectangle.Top + 123, () => Right(false)))
            {
                //when mistake arrive right wall 2.1 133, 132
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 133, maplestory.WindowRectangle.Top + 132, () => { Right(false); Climb(false); }))
                {
                    goto PartTwo;
                }
                else
                {
                    Right(true);
                    Climb(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until character arrive rope 2 top.");
            }
            else
            {
                Left(false);
                Climb(false);
                return;
            }
            //rope 2 top 85, 110
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 85, maplestory.WindowRectangle.Top + 110 , () => Climb(false))) { }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until the character arrive the right wall 3.1");
            }
            else
            {
                return;
            }
            PartThree:
            // right wall 1 = 136, 110
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 136, maplestory.WindowRectangle.Top + 110))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 133, maplestory.WindowRectangle.Top + 110))
                {
                    break;
                }
                else
                {
                    TwoKeyAttack(maplestory.Handle, Keys.Right, Keys.Control, Keys.Shift, 600, 600);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until the character arrive the potal.");

            }
            else
            {
                return;
            }
            //portal 127, 110
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 127, maplestory.WindowRectangle.Top + 110, () => Left(false)))
            {
                // when mistake arrive left wall 3.1 = 73, 110
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 73, maplestory.WindowRectangle.Top + 110, () => Left(false)))
                {
                    goto PartThree;
                }
                else
                {
                    Left(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until the character enter the portal.");
                Delay(400);
            }
            else
            {
                return;
            }
            do
            {
                if (EnterCounter++ > 10)
                {
                    EnterCounter = 0;
                    goto Restart;
                }
                else
                {
                    // portal right side 130, 110
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 130, maplestory.WindowRectangle.Top + 110))
                    {
                        EnterCounter = 0;
                        Left(50, 100);
                    }
                    // portal left side 123, 110
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 123, maplestory.WindowRectangle.Top + 110))
                    {
                        EnterCounter = 0;
                        Right(50, 100);
                    }
                    Up();
                }
            } while (!MiniMap.MapEnterCheck(maplestory.WindowRectangle.Left + 38, maplestory.WindowRectangle.Top + 72, 102, 85, 34));
            return;

            Restart:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until the character arrive left wall 3.1(re).");
            }
            else
            {
                return;
            }
            // left wall 3.1 73, 110
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 73, maplestory.WindowRectangle.Top + 110, () => Left(false)))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 76, maplestory.WindowRectangle.Top + 110, () => Left(false)))
                {
                    break;
                }
                else
                {
                    Left(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until the character arrive bottom platform(re).");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 73, maplestory.WindowRectangle.Top + 154))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 76, maplestory.WindowRectangle.Top + 154))
                {
                    break;
                }
                else
                {
                    JumpDown(maplestory.Handle, 1200);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage5: Wait until the character arrive left wall 1.1(re).");
            }
            else
            {
                return;
            }
            // left wall 1.1 61, 154
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 61, maplestory.WindowRectangle.Top + 154, () => Left(false)))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 64, maplestory.WindowRectangle.Top + 154, () => Left(false)))
                {
                    break;
                }
                else
                {
                    Left(true);
                }
            }
            goto PartOne;
        }

        public void KnightStrongholdStage6(Maplestory maplestory)
        {
            var EnterCounter = 0;

            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage6: Wait until enter the map.");
            }
            else
            {
                return;
            }
            //spawn point 54, 154
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 54, maplestory.WindowRectangle.Top + 154))
            {
                Delay(5);
            }
            PartOne:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage6: Wait until the character arrive the right wall 1.");
            }
            else
            {
                return;
            }
            // right wall 1 = 138, 154
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 135, maplestory.WindowRectangle.Top + 154))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 138, maplestory.WindowRectangle.Top + 154))
                {
                    break;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Right, Keys.Shift, 200);
                }
            }
            
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage6: Wait until character climb the rope 1.");
            }
            else
            {
                return;
            }
            //rope 1 120, 144
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 120, maplestory.WindowRectangle.Top + 144, () => Left(false)))
            {
                //when mistake arrive spawn point 54, 154
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 54, maplestory.WindowRectangle.Top + 154, () => { Left(false); Climb(false); }))
                {
                    goto PartOne;
                }
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 125, maplestory.WindowRectangle.Top + 154))
                {
                    Climb(true);
                }
                else
                {
                    Left(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage6: Wait until character arrive rope 1 top.");
            }
            else
            {
                Left(false);
                Climb(false);
                return;
            }
            //rope 1 top 120, 127
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 120, maplestory.WindowRectangle.Top + 127, () => { Climb(false); OneKeyAttack(maplestory.Handle, 0x00, Keys.D, 500); })) { }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage6: Wait until the character arrive the left wall 2.1.");
            }
            else
            {
                return;
            }
            //left wall 2.1 60, 127
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 63, maplestory.WindowRectangle.Top + 127))
            {
                if (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 60, maplestory.WindowRectangle.Top + 127))
                {
                    break;
                }
                else
                {
                    OneKeyAttack(maplestory.Handle, Keys.Left, Keys.Shift, 200);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage6: Wait until the character jumpdown.");
                Delay(500);
                Right(150, 300);
                JumpDown(maplestory.Handle, 200);
                JumpJump(maplestory.Handle, 0, 200);
            }
            else
            {
                return;
            }
            PartTwo:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage6: Wait until the character arrive the portal.");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 132, maplestory.WindowRectangle.Top + 154, () => Right(false)))
            {
                //when mistake arrive right wall 2.1 133, 127
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 133, maplestory.WindowRectangle.Top + 127, () => Right(false)))
                {
                    JumpDown(maplestory.Handle, 200);
                    goto PartTwo;
                }
                else
                {
                    Right(true);
                }
            }
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage6: Wait until the character enter the portal.");
                Delay(400);
            }
            else
            {
                return;
            }
            do
            {
                if (EnterCounter++ > 10)
                {
                    EnterCounter = 0;
                    goto Restart;
                }
                else
                {
                    // portal right side 136, 154
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 136, maplestory.WindowRectangle.Top + 154))
                    {
                        EnterCounter = 0;
                        Left(50, 100);
                    }
                    // portal left side 129, 154
                    while (IsBotting && MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 129, maplestory.WindowRectangle.Top + 1544))
                    {
                        EnterCounter = 0;
                        Right(50, 100);
                    }
                    Up();
                }
            } while (!MiniMap.MapEnterCheck(maplestory.WindowRectangle.Left + 38, maplestory.WindowRectangle.Top + 72, 102, 85, 34));
            return;

            Restart:
            if (IsBotting)
            {
                mGForm.UpdateLogLabel("KnightStrongholdStage6: Wait until the character arrive the left wall 1(re).");
            }
            else
            {
                return;
            }
            while (IsBotting && !MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 51, maplestory.WindowRectangle.Top + 154, () => Left(false)))
            {
                if (MiniMap.IsPlayerArriveAt(maplestory.WindowRectangle.Left + 54, maplestory.WindowRectangle.Top + 154, () => Left(false)))
                {
                    break;
                }
                else
                {
                    Left(true);
                }
            }
            goto PartOne;
        }
    }
}
