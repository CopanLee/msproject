﻿using System;
using System.Windows.Forms;
using MSProject.Library;
using MSProject.Script;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Collections;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace MSProject
{
    public partial class MGForm : Form
    {
        private Hashtable hotkeyTable;
        private ScriptManager scriptManager;

        public MGForm()
        {
            InitializeComponent();
            InitializeHotkey();
        }

        public delegate void InvokeLabel(string text);

        private void InitializeHotkey()
        {
            scriptManager = new ScriptManager(this);
            hotkeyTable = new Hashtable
            {
                { Hotkey.RegisterHotkey(Handle, 0, Keys.F3), Keys.F3 },
                { Hotkey.RegisterHotkey(Handle, 2, Keys.F3), Keys.F3 },
                { Hotkey.RegisterHotkey(Handle, 0, Keys.F6), Keys.F6 },
                { Hotkey.RegisterHotkey(Handle, 2, Keys.F6), Keys.F6 }
            };
        }

        protected override void WndProc(ref Message message)
        {
            base.WndProc(ref message);
            if (message.Msg == 0x312)
            {
                var key = (Keys)hotkeyTable[message.WParam.ToInt32()];
                
                Thread.Sleep(500);
                switch (key)
                {
                    case Keys.F3:
                        scriptManager.ScriptChoose(Script.Script.DreamyForestTrail);
                        break;
                    case Keys.F6:
                        scriptManager.ScriptChoose(Script.Script.KnightStronghold);
                        break;
                }
            }
        }

        public void UpdateLogLabel(string text)
        {
            if (LogLabel.InvokeRequired)
            {
                Invoke(new InvokeLabel(UpdateLogLabel), text);
            }
            else
            {
                LogLabel.Text = text;
            }
        }

        public void UpdateRunNumberLabel(string time)
        {
            if (RunNumberLabel.InvokeRequired)
            {
                Invoke(new InvokeLabel(UpdateRunNumberLabel), time);
            }
            else
            {
                RunNumberLabel.Text = time.ToString();
            }
        }

        public void ShowMessageBox(string title, string text)
        {
            MessageBox.Show(text, title, MessageBoxButtons.OK);
        }

    }
}
